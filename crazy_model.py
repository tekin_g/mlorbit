############################################################################
# File that defines the "Crazy" Model (Autoencoder Based Design)
# Should only be used for importing the model, has no training capability.
# Written by Guney & Commented by ChatGPT
#############################################################################

import tensorflow as tf  # Import TensorFlow for machine learning
import numpy as np  # Import NumPy for numerical operations
from keras import Input  # Import Input from Keras for model inputs
from keras.src.regularizers import regularizers  # Import regularizers for regularization
from tensorflow.keras.layers import Layer, Dense, Dropout, BatchNormalization  # Import layers from Keras
from tensorflow.keras.models import Model  # Import Model from Keras


def calculate_shapes(input_shape, latent_dim, num_layers):
    """
    Calculate the shapes of each layer in the encoder and decoder.
    Uses geometric spacing between input shape and latent dimension.

    Args:
    input_shape (int): The dimensionality of the input data.
    latent_dim (int): The dimensionality of the latent space.
    num_layers (int): The number of layers in the encoder/decoder.

    Returns:
    List[int]: A list of layer shapes.
    """
    if num_layers == 1:
        return [latent_dim]
    shapes = np.geomspace(input_shape, latent_dim, num_layers).astype(int)
    return shapes


class Autoencoder(Model):
    def __init__(self, config, **kwargs):
        super(Autoencoder, self).__init__(**kwargs)
        self.input_dim = config['input_dim']  # Input dimensionality
        self.latent_dim = config['latent_dim']  # Latent space dimensionality
        self.depth = config['encoder_depth']  # Number of layers in the encoder
        self.encoder_structure = calculate_shapes(self.input_dim, self.latent_dim, self.depth)  # Encoder layer sizes
        self.decoder_structure = self.encoder_structure[-1::-1]  # Decoder layer sizes (inverted version of encoder layer sizes)

        if len(self.encoder_structure) == 1:  # Fix for single-layer autoencoder
            self.decoder_structure = [self.input_dim]

        # Build the encoder
        encoder_layers = []
        encoder_layers.append(Input((self.input_dim,)))  # Input layer
        encoder_layers.append(BatchNormalization(name='batch_e1'))  # Batch normalization layer
        encoder_layers.append(Dense(self.encoder_structure[0], activation=config['activation'],
                                    kernel_regularizer=regularizers.L2(config['l2_reg'])))  # Dense layer

        for i in self.encoder_structure[1:]:
            encoder_layers.append(BatchNormalization(name=f'batch_e{i}'))  # Batch normalization
            encoder_layers.append(Dropout(config['dropout']))  # Dropout layer
            encoder_layers.append(Dense(i, activation=config['activation'],
                                        kernel_regularizer=regularizers.L2(config['l2_reg'])))  # Dense layer

        self.encoder = tf.keras.Sequential(encoder_layers)  # Combine encoder layers into a sequential model

        # Build the decoder
        decoder_layers = []
        decoder_layers.append(BatchNormalization(name='batch_d1'))  # Batch normalization layer
        decoder_layers.append(Dense(self.decoder_structure[0], activation=config['activation'],
                                    kernel_regularizer=regularizers.L2(config['l2_reg'])))  # Dense layer

        for i in self.decoder_structure[1:]:
            decoder_layers.append(BatchNormalization(name=f'batch_d{i}'))  # Batch normalization
            decoder_layers.append(Dropout(config['dropout']))  # Dropout layer
            decoder_layers.append(Dense(i, activation=config['activation'],
                                        kernel_regularizer=regularizers.L2(config['l2_reg'])))  # Dense layer

        self.decoder = tf.keras.Sequential(decoder_layers)  # Combine decoder layers into a sequential model

    def call(self, x, training=False):
        """
        Forward pass through the autoencoder.

        Args:
        x (Tensor): Input tensor.
        training (bool): Whether the model is in training mode.

        Returns:
        Tuple[Tensor, Tensor]: Encoded and decoded representations.
        """
        encoded = self.encoder(x, training=training)  # Encode the input
        decoded = self.decoder(encoded, training=training)  # Decode the encoded representation
        return encoded, decoded

    def get_config(self):
        """
        Get the configuration of the autoencoder.
        Required for .keras saving.

        Returns:
        Dict: Configuration dictionary.
        """
        return dict(config=self.config)


class CrazyModel(tf.keras.Model):
    def __init__(self, config, **kwargs):
        super(CrazyModel, self).__init__(**kwargs)
        self.autoencoder = Autoencoder(config, **kwargs)  # Instantiate the autoencoder
        self.config = config
        width = config['network_width']  # Width of the dense layers for corrector calculation
        layers = []
        layers.append(BatchNormalization(name='batch_f'))  # Batch normalization layer
        layers.append(Dense(width, activation=config['activation'],
                            kernel_regularizer=regularizers.L2(config['l2_reg'])))  # Dense layer

        for i in range(config['network_depth'] - 1):
            layers.append(BatchNormalization(name=f'batch_f{i}'))  # Batch normalization
            layers.append(Dropout(config['dropout']))  # Dropout layer
            layers.append(Dense(width, activation=config['activation'],
                                kernel_regularizer=regularizers.L2(config['l2_reg'])))  # Dense layer

        self.final = tf.keras.Sequential(layers)  # Combine layers into a sequential model for corrector calculation

    def call(self, x, training=False):
        """
        forward pass through the crazy model.
        Diagram:

        BPM -> encoder -> latent space -> dense layers (self.final) -> corrector prediction
                             ↳  decoder -> BPM reconstruction

        args:
        x (tensor): input tensor.
        training (bool): whether the model is in training mode.

        returns:
        tuple[tensor, tensor]: decoded output and final layer output.
        """
        encoded, decoded = self.autoencoder(x, training=training)  # Pass through autoencoder
        return decoded, self.final(encoded, training=training)  # Pass encoded representation through final layers

    def get_config(self):
        """
        Get the configuration of the crazy model.

        Returns:
        Dict: Configuration dictionary.
        """
        return dict(config=self.config)


class CrazyModelCorr(tf.keras.Model):
    def __init__(self, config, **kwargs):
        super(CrazyModelCorr, self).__init__(**kwargs)
        self.autoencoder = Autoencoder(config, **kwargs)  # Instantiate the autoencoder
        self.config = config
        width = config['network_width']  # Width of the dense layers for corrector strength calculation
        layers = []
        layers.append(BatchNormalization(name='batch_f'))  # Batch normalization layer
        layers.append(Dense(width, activation=config['activation'],
                            kernel_regularizer=regularizers.L2(config['l2_reg'])))  # Dense layer

        for i in range(config['network_depth'] - 1):
            layers.append(BatchNormalization(name=f'batch_f{i}'))  # Batch normalization
            layers.append(Dropout(config['dropout']))  # Dropout layer
            layers.append(Dense(width, activation=config['activation'],
                                kernel_regularizer=regularizers.L2(config['l2_reg'])))  # Dense layer

        self.final = tf.keras.Sequential(layers)  # Combine layers into a sequential model

    def call(self, x, training=False):
        """
        Forward pass through the crazy model with correlations.
        Diagram:

        Initial Corrector Strengths  -------↴
        bpm-> encoder -> latent space -> dense layers (self.final) -> corrector prediction
                             ↳  decoder -> BPM reconstruction
        Args:
        x (Tuple[Tensor, Tensor]): Tuple containing the input tensor and additional data.
        training (bool): Whether the model is in training mode.

        Returns:
        Tuple[Tensor, Tensor]: Decoded output and final layer output with additional data concatenated.
        """
        encoded, decoded = self.autoencoder(x[0], training=training)  # Pass first element of tuple through autoencoder
        input = tf.concat([encoded, x[1]], axis=-1)  # Concatenate encoded representation with second element of tuple
        return decoded, self.final(input, training=training)  # Pass concatenated data through final layers

    def get_config(self):
        """
        Get the configuration of the crazy model with correlations.

        Returns:
        Dict: Configuration dictionary.
        """
        return dict(config=self.config)
