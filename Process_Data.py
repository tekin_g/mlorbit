import matplotlib
import numpy as np
import pandas as pd

matplotlib.use('Agg')

import tensorflow as tf

tf.keras.backend.set_floatx('float64')
from tensorflow.keras.utils import Progbar

# from IPython.display import display, Latex

import os

########################### Inputs ###########################


# flag to do the conversion of the units
model_pyAT = True
corr_version = True  # opportunely modified in the code reading a file if necessary
# flag ot save or not the model -> not used anymore. I use the Results file.
datset_size = 400000

#############################################################

seed = 123
np.random.seed(seed)
tf.random.set_seed(seed)

my_path = 'data/Network_3b/Run_3'
datafile_list = os.listdir(my_path)

data_input_pre = pd.DataFrame()
data_output_pre = pd.DataFrame()
data_output_new_pre = pd.DataFrame()

range_files = np.arange(0, len(datafile_list), 1)

# f = h5py.File(os.path.join(my_path,datafile_list[0]))
# if 'input_new_corr_0' in f.keys():
corr_version = True
# name_out_new = pd.read_hdf(os.path.join(my_path,datafile_list[index]),'input_new_corr_0')
pg = Progbar(datset_size)

for index in range_files:
    # print(datafile_list[index])

    name_in = pd.read_hdf(os.path.join(my_path, datafile_list[index]), 'input')
    name_out_new = pd.read_hdf(os.path.join(my_path, datafile_list[index]), 'input_new_corr_0')
    name_out_new.columns = name_out_new.columns.str.replace("ARS", "START_ARS", regex=True)

    # f = h5py.File(os.path.join(my_path,datafile_list[index]))
    # if 'input_new_corr_0' in f.keys():
    #        name_out_new = pd.read_hdf(os.path.join(my_path,datafile_list[index]),'input_new_corr_0')

    name_out = pd.read_hdf(os.path.join(my_path, datafile_list[index]), 'output')  # _fast
    # name_charge = pd.read_hdf(os.path.join(my_path,datafile_list[index]),'charge')

    if model_pyAT == False:
        # I mix VA- and SB- channels
        name_in.columns = name_in.columns.str.replace("AA-", "SB-", regex=True)
        name_out.columns = name_out.columns.str.replace("AA-", "SB-", regex=True)
        name_in.columns = name_in.columns.str.replace("VA-", "SB-", regex=True)
        name_out.columns = name_out.columns.str.replace("VA-", "SB-", regex=True)

    # if 'input_new_corr_0' in f.keys():
    #	name_out_new.columns = name_out_new.columns.str.replace("AA-","SB-",regex=True)
    #	name_out_new.columns = name_out_new.columns.str.replace("VA-","SB-",regex=True)
    #        name_out_new.columns = name_out_new.columns.str.replace(":I-SET","_0:I-SET",regex=True)
    # name_charge.columns = name_charge.columns.str.replace("AA-","SB-",regex=True)

    # I remove a corrupted file
    # if max(name_in.describe().loc['max'])>0.01:
    #        print(datafile_list[index])
    #        print(max(name_in.describe().loc['max']))
    #        quit()

    data_input_pre = pd.concat([data_input_pre, name_in - 0 * name_in.iloc[0]], ignore_index=True,
                               verify_integrity=True)
    data_output_pre = pd.concat([data_output_pre, name_out - 0 * name_out.iloc[0]], ignore_index=True,
                                verify_integrity=True)
    data_output_new_pre = pd.concat([data_output_new_pre, name_out_new], ignore_index=True, verify_integrity=True)
    pg.add(name_in.shape[0])
    if data_output_pre.shape[0] > datset_size:
        break
    # if 'input_new_corr_0' in f.keys():
#        data_output_new_pre = pd.concat([data_output_new_pre,name_out_new])

# if 'input_new_corr_0' in f.keys():
#    data_output_pre = pd.concat([data_output_pre,data_output_new_pre],axis=1)
# else:
#    data_output_pre = data_output_pre

data_output_pre = pd.concat([data_output_new_pre, data_output_pre], axis=1)
data_sub_in = data_input_pre
data_sub_out = data_output_pre

# I swap input and output
output1 = data_sub_in  # -data_sub_in.iloc[0]
input1 = data_sub_out  # -data_sub_out.iloc[0]

print('Start input: ' + str(input1.shape))
print('Start output: ' + str(output1.shape))

###############################I SWAPPED CORRECTORS AND ORBIT TO OBTAIN WHICH CORRECTORS WILL GIVE ME A CERTAIN ORBIT

print('Start input: ' + str(input1.shape))
print('Start output: ' + str(output1.shape))

if model_pyAT:

    ch_x = []
    for i in output1.keys():
        if (i.find('MCOX') != -1):
            ch_x.append(i)
    corr_x = output1.keys()[0:len(ch_x)]
    corr_y = output1.keys()[len(ch_x):len(output1.keys())]
    output1[corr_x] = output1[corr_x] * 5 / 6 * 1e4  # from rad -> A (5 A <-> 600 urad)
    output1[corr_y] = output1[corr_y] * 5 / 4 * 1e4  # from rad -> A (5 A <-> 400 urad)

    if corr_version:
        ch_x = []
        ch_y = []
        ch_bpm = []
        for i in input1.keys():
            if (i.find('MCOX') != -1):
                ch_x.append(i)
            elif (i.find('MCOY') != -1):
                ch_y.append(i)
            else:
                ch_bpm.append(i)

        input1[ch_x] = input1[ch_x] * 5 / 6 * 1e4  # from rad -> A (5 A <-> 600 urad)
        input1[ch_y] = input1[ch_y] * 5 / 4 * 1e4  # from rad -> A (5 A <-> 400 urad)
        # I convert rad to mA, and m to mm (version VA).
        input1[ch_bpm] = input1[ch_bpm] * 1e3  # from m to mm
    else:
        input1 = input1 * 1e3  # from m to mm

        # input1_0 = input1[input1.keys()[0:len(corr_x+corr_y)]].add_suffix('_0')
        # input1 = pd.concat([input1_0, input1],axis=0)

output1_new = output1
input1_new = input1
del input1
del output1

# del output1
# output1 = output1_new

input1_keys = input1_new.keys()
output1_keys = output1_new.keys()

result = pd.concat([input1_new, output1_new], axis=1)

result.dropna(axis=0, inplace=True)  # I remove the NaN

# kk = result.keys()
# result_new = result[result[kk[230:230+230]].apply(lambda row: all(float(column) <= 500 for column in row), axis=1)]

# result_new = result[result.apply(lambda row: all(float(column) <= 1 for column in row), axis=1)]

result = result.sample(frac=1, axis=0)  # I shuffle the data

input1_keys_ok = []
output1_keys_ok = []

count = -1
for ch in result.keys():
    count = count + 1
    check_sanity = sum(np.abs(np.diff(result[ch])))

    if check_sanity == 0.:
        print(check_sanity)
        # The channel does not change its value -> I delete the full column
        print('Drop: ' + ch)
        result.drop(ch, axis='columns', inplace=True)

    else:
        if ch in list(input1_keys):
            input1_keys_ok.append(ch)
        elif ch in list(output1_keys):
            output1_keys_ok.append(ch)

input1 = result[input1_keys_ok]
output1 = result[output1_keys_ok]

print("=============Input=====================")
print(input1.head())
print(input1.shape)
print("=============output=====================")
print(output1.head())
print(output1.shape)

input1.to_hdf("run3_dataset_{}.h5".format(datset_size), mode="w", key="input")
output1.to_hdf("run3_dataset_{}.h5".format(datset_size), mode="a", key="output", append=True)
