##########################################################################################
# Same functionality as train_crazy_model.py except with initial corrector strengths.
# For more detailed comments refer to the other file
# written by Guney & some parts based on ML_Train.py by Simona
###########################################################################################


from datetime import datetime

import keras
import matplotlib
########################### Import ###########################
import numpy as np
import pandas as pd

# CODE TO TRAIN A MODEL IN LINUX (SCRIPT VERSION)

matplotlib.use('Agg')
import matplotlib.pyplot as plt

import tensorflow as tf

tf.keras.backend.set_floatx('float32')

from scipy.fft import fft

# from IPython.display import display, Latex

import random

import os

import h5py
from helpers import *
from crazy_model import CrazyModelCorr
from tensorflow.keras.utils import Progbar

#############################################################


########################### I/O ###########################
make_plot = True  # use the same flag. In case of a cluster run, savefig
corr_version = True  # opportunely modified in the code reading a file if necessary

now = datetime.now()
timestamp_str = now.strftime("%Y-%m-%d-%H%M%S/")
output_path = "out/" + timestamp_str
if not os.path.exists(output_path):  # create output path if it does not exists
    os.makedirs(output_path)

######################## Settings ###############################
n_epochs = 100
n_batch_size = 128
l_rate = 5e-4
l2_reg_pen_loop = 1e-7
internal_dropout_rate = 0.3

no_data_token = -5  #Token that represnts the broken BPM
ratio_input_dropout = 0  # [0,1] ratio of the datapoints that contain broken BPMs
dropout_column_no = 230  # how many BPMs can actually be broken ( in each datapoint only a single one is actually
# broken, this just determines how many BPMs to select from when marking as broken)
alpha = 2.0  # ratio of reconstruction loss to corrector loss

##### Settings for the corrector calculating part of the network
n_depth = 2
n_width = 230
#### Settings for the AutoEncoder
latent_dim = 30
encoder_depth = 2

print('n depth: ' + str(n_depth))
print('n batch size: ' + str(n_batch_size))
print('learning rate: ' + str(l_rate))
print('l2 regurarization: ' + str(l2_reg_pen_loop))

# Set a random seed to make the script reproducible
seed = 123
np.random.seed(seed)
tf.random.set_seed(seed)

dataset = "run_3_casecorr_200000.h5"
input1 = pd.read_hdf(dataset, key="input")
output1 = pd.read_hdf(dataset, key="output")

# print(input1.shape)
# print(output1.shape)
if not corr_version:  # If network 1 drop initial correctors.
    columns_to_drop = [col for col in input1.columns if 'MCOX' in col]
    for col in columns_to_drop:
        print(f"Dropping {col} with mean: {input1[col].mean()}")

    df = input1.drop(columns=columns_to_drop)

print("============Input============================")

print(input1.head())
print("============Output============================")

print(output1.head())
# split the data
n_splits = 100  # 10
input_train, input_val, output_train, output_val = split_dataSet(input1, output1, n_splits)

dimX = input_train.shape[1]
dimY = output_train.shape[1]






print(len(input_train))
print(len(output_train))
print('------------------------')
print(len(input_val))
print(len(output_val))
# print('------------------------')
# print(len(input_train2))
# print(len(output_train2))
# print('------------------------')
# print(len(input_val2))
# print(len(output_val2))
# print('------------------------')


# Preprocess data - Scale to [0,1]


# Depending on the data you might need some scaling, especially, if the input data have various ranges
scale_min = 0
scale_max = 1

output_min = np.min(output_train, axis=0)
output_max = np.max(output_train, axis=0)
input_min = np.min(input_train, axis=0)
input_max = np.max(input_train, axis=0)
# To be saved for the scaling in the prediction code
orbit_min = input_min  # np.min(input_train, axis = 0)
orbit_max = input_max  # np.max(input_train, axis = 0)

corr_min = output_min  # np.min(output_train, axis = 0)
corr_max = output_max  # np.max(output_train, axis = 0)

hf = h5py.File(output_path + 'Limits_scale.h5', 'w')
hf.create_dataset('orbit_min', data=orbit_min)
hf.create_dataset('orbit_max', data=orbit_max)
hf.create_dataset('corr_min', data=corr_min)
hf.create_dataset('corr_max', data=corr_max)

hf.close()
print('Limits for the scaling saved (Limits_scale.h5)')

# Suggested by Romana (21/09/2023)
# output_min2 = np.min(output_train2, axis = 0)
# output_max2 = np.max(output_train2, axis = 0)
# input_min2 = np.min(input_train2, axis = 0)
# input_max2 = np.max(input_train2, axis = 0)


output_train_df = pd.DataFrame(data=output_train, columns=output1.columns)

input_train_df = pd.DataFrame(data=input_train, columns=input1.columns)

max_values_out = output_train_df.describe(include='all').loc['max'].values
min_values_out = output_train_df.describe(include='all').loc['min'].values
std_values_out = output_train_df.describe(include='all').loc['std'].values

max_values_in = input_train_df.describe(include='all').loc['max'].values
min_values_in = input_train_df.describe(include='all').loc['min'].values
std_values_in = input_train_df.describe(include='all').loc['std'].values

output_train_scaled = scaling(output_train, output_min, output_max, scale_min, scale_max).astype(np.float32)
output_val_scaled = scaling(output_val, output_min, output_max, scale_min, scale_max).astype(np.float32)

input_train_scaled = scaling(input_train, input_min, input_max, scale_min, scale_max).astype(np.float32)
input_val_scaled = scaling(input_val, input_min, input_max, scale_min, scale_max).astype(np.float32)
print("BPMs:", input1.columns[230:])
print("Corrs: ", input1.columns[:230])
input_train_dropped = np.copy(input_train_scaled).astype(np.float32)[:, 230:]
input_train_corr = np.copy(input_train_scaled).astype(np.float32)[:, :230]

if not dropout_column_no == 0:
    dropout_columns = np.random.choice(input_train_scaled.shape[1], dropout_column_no,
                                       replace=False)  # choose which collumns could be dropped
    print("dropping: ", dropout_columns)
    # length = input_train_dropped.shape[0]
    # input_train_dropped = np.tile(input_train_dropped,(dropout_column_no+1,1))
    # input_train_scaled = np.tile(input_train_scaled,(dropout_column_no+1,1))

    # for i,col in enumerate(dropout_columns):
    #    input_train_dropped[(i+1)*length:(i+2)*length,col]=no_data_token

    random_indices_train = np.random.choice(input_train.shape[0], int(ratio_input_dropout * input_train.shape[0]),
                                            replace=False)  # choose rows

    for row in random_indices_train:
        col = np.random.choice(dropout_columns, 1, replace=False)
        input_train_dropped[row, col] = no_data_token



# These are the hyperparameters, that you can tune to increase the performance of your network
config = {'network_depth': n_depth,
          'network_width': n_width,  # 230
          'activation': 'elu',  # relu
          'l2_reg': l2_reg_pen_loop,  # 0.00005. It was 0.001
          'learning_rate': l_rate,
          # 5e-5 #Small number->finer steps (longer time) optimization. Maybe to be re-adjusted epoch
          'batch_size': n_batch_size,
          'epoch': n_epochs,
          'dropout': internal_dropout_rate,
          'no_data_token': no_data_token,
          'dropout_column': dropout_column_no,
          'input_dim': 230,
          'latent_dim': latent_dim,
          'encoder_depth': encoder_depth,
          'alpha': alpha,
          'training_data_ratio': ratio_input_dropout}  # 30000

nice_string = ""
for key, value in config.items():
    nice_string += f"{key}: {value}\n"

file_path = output_path + 'config.txt'
with open(file_path, 'w') as file:
    file.write(nice_string)

model = CrazyModelCorr(config)

optimizer = keras.optimizers.Adam(config['learning_rate'])
split_index = int(input_train_scaled.shape[0] * 0.2)
training_dataset = tf.data.Dataset.from_tensor_slices((
                                                      input_train_dropped[split_index:], input_train_corr[split_index:],
                                                      input_train_scaled[split_index:, 230:],
                                                      output_train_scaled[split_index:]))
val_dataset = tf.data.Dataset.from_tensor_slices((input_train_dropped[:split_index], input_train_corr[:split_index],
                                                  input_train_scaled[:split_index, 230:],
                                                  output_train_scaled[:split_index]))
print(split_index, len(training_dataset), len(val_dataset))

loss1 = tf.keras.losses.MeanSquaredError()
loss2 = tf.keras.losses.MeanSquaredError()

train_metric_1 = tf.keras.metrics.MeanSquaredError(name="OutputMSE")
train_metric_2 = tf.keras.metrics.MeanSquaredError(name="ReconstructionMSE")
train_metric_3 = tf.keras.metrics.MeanAbsolutePercentageError(name="ReconstructionMAP")
train_metric_4 = tf.keras.metrics.R2Score(name="OutputR2")

val_metric_1 = tf.keras.metrics.MeanSquaredError(name="ValOutputMSE")
val_metric_2 = tf.keras.metrics.MeanSquaredError(name="ValReconstructionMSE")
val_metric_3 = tf.keras.metrics.MeanAbsolutePercentageError(name="ValReconstructionMAP")
val_metric_4 = tf.keras.metrics.R2Score(name="ValOutputR2")


@tf.function
def lossfn(input_reconstruction, y, y_true, x_true):
    rec_loss = loss1(input_reconstruction, x_true)
    output_loss = loss1(y, y_true)
    return rec_loss + alpha * output_loss


@tf.function
def training_step(model, optimizer, device, input_dropped, input_train_corr, input_not_dropped, output):
    with tf.GradientTape() as tape, tf.device(device):
        reconstruction, y = model((input_dropped, input_train_corr), training=True)
        # print(reconstruction.device)
        loss = lossfn(reconstruction, y, output, input_not_dropped)
        # print(loss.device)
        l2_loss = tf.reduce_sum(model.losses)
        loss += 1. * l2_loss
    gradients = tape.gradient(loss, model.trainable_variables)
    # print(gradients[0].device)
    optimizer.apply_gradients(zip(gradients, model.trainable_variables))

    train_metric_1.update_state(y, output)
    train_metric_2.update_state(reconstruction, input_not_dropped)
    train_metric_3.update_state(reconstruction, input_not_dropped)
    train_metric_4.update_state(y, output)
    return loss


history = {'loss': [], 'val_loss': []}

gpu_devices = tf.config.list_logical_devices('GPU')
device = '/CPU:0' if len(gpu_devices) == 0 else gpu_devices[0]
print(device)
for i in range(config['epoch']):
    print(f"epoch: {i}")
    training_dataset_loc = training_dataset.shuffle(32, seed=seed).batch(n_batch_size)
    length = len(training_dataset_loc)  # shuffle the training dataset
    training_dataset_loc = training_dataset_loc.apply(tf.data.experimental.copy_to_device('/GPU:0'))
    val_dataset_loc = val_dataset.shuffle(32, seed=seed).batch(n_batch_size)  # shuffle the training dataset
    train_metric_1.reset_state()
    train_metric_2.reset_state()
    train_metric_3.reset_state()
    train_metric_4.reset_state()

    val_metric_1.reset_state()
    val_metric_2.reset_state()
    val_metric_3.reset_state()
    val_metric_4.reset_state()

    loss_sum = 0
    step = 0
    pg = Progbar(length,
                 stateful_metrics=["loss", "Output MSE", "Reconstruction MSE", "Reconstruction MAP", "Output R2"])
    for input_dropped, input_train_corr, input_not_dropped, output in training_dataset_loc:
        # if step == 0:
        #    print(input_dropped.device)
        #    print(input_not_dropped.device)
        #    print(output.device)
        loss = training_step(model, optimizer, '/GPU:0', input_dropped, input_train_corr, input_not_dropped, output)
        values = [("loss", loss), ("Output MSE", train_metric_1.result()),
                  ("Reconstruction MSE", train_metric_2.result()),
                  ("Reconstruction MAP", train_metric_3.result()), ("Output R2", train_metric_4.result())]
        pg.add(1, values=values)
        loss_sum += loss
        step += 1

    history['loss'].append(loss_sum.numpy() / step)
    pg = Progbar(len(val_dataset_loc),
                 stateful_metrics=["val Output MSE", "val Reconstruction MSE", "val Reconstruction MAP",
                                   "val Output R2"])
    # print(len(val_dataset_loc))
    loss_sum = 0
    step = 0
    for input_dropped, input_train_corr, input_not_dropped, output in val_dataset_loc:
        reconstruction, y = model((input_dropped, input_train_corr), training=False)
        loss = lossfn(reconstruction, y, output, input_not_dropped)
        val_metric_1.update_state(y, output)
        val_metric_2.update_state(reconstruction, input_not_dropped)
        val_metric_3.update_state(reconstruction, input_not_dropped)
        val_metric_4.update_state(y, output)
        values = [("val_loss", loss), ("val Output MSE", val_metric_1.result()),
                  ("val Reconstruction MSE", val_metric_2.result()),
                  ("val Reconstruction MAP", val_metric_3.result()), ("val Output R2", val_metric_4.result())]

        pg.add(1, values=values)
        step += 1
        loss_sum += loss
    history['val_loss'].append(loss_sum.numpy() / step)

model.save(output_path + 'my_model_try.keras')



if make_plot:
    font = 20
    plt.figure(figsize=(8, 5))
    plt.plot(history['loss'], '-')

    plt.xlabel('Epochs', fontsize=font)
    plt.ylabel('Loss', fontsize=font)
    plt.tick_params(labelsize=font)
    plt.yscale('log')
    plt.grid(axis="y", which='minor')
    plt.grid(axis="x", which='major')

    print(history['loss'][-1])
    # plt.show()
    plt.savefig(output_path + 'loss.png')
    plt.close()

    plt.figure()
    plt.plot(history['loss'])
    plt.plot(history['val_loss'])
    plt.yscale('log')
    plt.title('Model loss')
    plt.ylabel('Loss')
    plt.xlabel('Epoch')

    plt.legend(['Loss', 'Val_loss'], loc='upper right')
    # plt.show()
    plt.savefig(output_path + 'loss_val_loss.png')
    print(history['val_loss'][-1])
    print(np.min(history['val_loss']))

# here you use the network for predicting
_, output_pred_scaled = model.predict((input_val_scaled[:, 230:], input_val_scaled[:, :230]))
output_pred_scaled = pd.DataFrame(data=output_pred_scaled, columns=output1.columns)

############## NOT CLEAR IN THE NEW SETUP IF THIS IS OK ##############

# Trying with the validation data
# output_pred_scaled2 = surr.predict(input_val_scaled2)
# output_pred_scaled2 = pd.DataFrame(data=output_pred_scaled2, columns=output1.columns)

# output_pred2 = scaling_reverse(output_pred_scaled2,output_min,output_max,scale_min,scale_max)
# output_val2 = pd.DataFrame(data=output_val2, columns= output1.columns)

'''if make_plot:
	plt.figure(figsize=(25,50))
	for ii in np.arange(1,36,1):
		key_sel = output_pred2.keys()[ii]
		plt.subplot(18,2,ii)
		plt.plot(output_val2[key_sel],'.-',label='measurement')
		plt.plot(output_pred2[key_sel],'.-',label='prediction')
		plt.title(key_sel)
		plt.legend()
	#plt.show()
	plt.savefig(output_path +'no_save.png')'''

# Rescale the predicted qoi
output_pred = scaling_reverse(output_pred_scaled, output_min, output_max, scale_min, scale_max)

output_val = pd.DataFrame(data=output_val, columns=output1.columns)

if make_plot:
    plt.figure(figsize=(25, 50))
    for ii in np.arange(1, 60, 1):
        key_sel = output_pred.keys()[ii]
        plt.subplot(30, 2, ii)
        plt.plot(output_val[key_sel], '.-', label='measurement')
        plt.plot(output_pred[key_sel], '.-', label='prediction')
        plt.title(key_sel)
        plt.legend()
    # plt.show()
    plt.savefig(output_path + 'no_save.png')

res = output_val - output_pred
res.describe()

# here you evaluate the performance of the network
train_metric_1 = RSquaredSeparated()

r2 = train_metric_1.call(output_val.values, output_pred.values).numpy()
r2 = pd.Series(data=r2, index=output1.columns)
r2 = np.round(r2, decimals=5)
r2 = pd.DataFrame(r2).T

if make_plot:
    plt.figure(figsize=(10, 6))
    plt.plot(r2.iloc[0], '*-')
    plt.xticks(ticks=np.arange(0, len(r2[:].T), 5), labels=r2.keys()[0::5], rotation=90)
    # plt.show()
    plt.savefig(output_path + 'fig_0.png')

    plt.plot(output_val.iloc[:] - 0 * output_val.iloc[0])
    plt.ylabel('I (A)')
    plt.xlabel('Shot')
    plt.title('Validation data')
    # plt.show()
    plt.savefig(output_path + 'fig_1.png')

    plt.figure(figsize=(10, 6))
    plt.subplot(2, 2, 1)
    plt.plot(output_val[:].T)
    plt.title('Machine')
    plt.ylabel('Correctors strength (A)')
    plt.xlabel('Correctors')
    plt.xticks(' ')
    plt.subplot(2, 2, 2)
    plt.title('Machine')
    plt.plot(output_val[:] - 0 * output_val.iloc[0])
    plt.ylabel('I (A)')
    plt.xlabel('Shot #')

    plt.subplot(2, 1, 2)
    plt.title('Model vs Machine')
    plt.subplots_adjust(hspace=0.4)
    plt.plot((output_val[:].T - output_pred[:].T))
    plt.ylabel('$I_{machine}-I_{model} (A)$')
    plt.xticks(np.arange(0, len(output_pred[:].T), 5), rotation=90)
    plt.yscale('log')
    # plt.show()
    plt.savefig(output_path + 'fig_2.png')

rel_error = (output_pred - output_val) / output_val * 100.
rel_error = pd.DataFrame(rel_error, columns=output1.columns).abs()
res = (output_pred - output_val)
res.describe()

if make_plot:
    plt.figure()
    plt.contourf(res)
    plt.xlabel('input')
    plt.xlabel('Corrector')
    plt.ylabel('Shot')
    plt.colorbar()
    # plt.show()
    plt.savefig(output_path + 'res.png')

std_values_out = max_values_out - min_values_out

ch_x = []
for i in output_val.keys():
    '''if(i.find('X-AVG') != -1):
        ch_x.append(output_val[i])'''
    if (i.find(':X') != -1):
        ch_x.append(output_val[i])
    '''if(i.find('CH') != -1):
        ch_x.append(output_val[i])'''

index_check = random.randint(0, output_val.shape[0] - 1)  # 28
scalin = 1

# ch_x = [val for key, val in output_val.items() if 'X-AVG' in key]
corr_x = output_val.keys()[0:len(ch_x)]
corr_y = output_val.keys()[len(ch_x):len(output_val.keys())]
# rel_error[keys[11:20]]

if make_plot:
    plt.figure(figsize=(12, 10))
    plt.subplots_adjust(wspace=0.5, hspace=0.4)
    plt.subplot(2, 1, 1)
    val_mean = output_val.describe(include='all').loc['mean'].values
    x = np.arange(0, len(output_val.keys()), 1)
    y1 = (output_val.iloc[index_check] - 0 * output_val.iloc[0, :] - std_values_out) * 1e3
    y2 = (output_val.iloc[index_check] - 0 * output_val.iloc[0, :] + std_values_out) * 1e3

    plt.fill_between(x, y1, y2, alpha=0.3)
    plt.plot(x, (output_pred.iloc[index_check] - 0 * output_val.iloc[0] - 0 * val_mean) * 1e3, label='Predition')
    plt.title('Predicted by the model')
    plt.xlabel('Correctors')
    plt.ylabel('I (mA)')
    plt.plot(x, (output_val.iloc[index_check] - 0 * output_val.iloc[0, :] - 0 * val_mean) * 1e3 * scalin, '.'
             , label='Validation')
    plt.xlabel('Corrector')
    plt.ylabel('$\Delta I (mA)$')
    plt.title('Used for the validation. Index = ' + str(index_check))
    plt.legend()

    plt.subplot(2, 1, 2)
    plt.title('Error of the model')
    plt.plot(res.iloc[index_check] * 1e3, alpha=0.5)
    plt.xticks(np.arange(0, len(res[:].T), 5), rotation=90)
    plt.xlabel('Corrector')
    plt.ylabel('I (mA)')
    plt.tight_layout()
    # plt.show()
    plt.savefig(output_path + 'shot.png')

ch_x = []
for i in output_val.keys():
    '''if(i.find('X-AVG') != -1):
        ch_x.append(output_val[i])'''
    '''if(i.find('CH') != -1):
        ch_x.append(output_val[i])'''
    if (i.find('MCOX') != -1):
        ch_x.append(output_val[i])

    # ch_x = [val for key, val in output_val.items() if 'X-AVG' in key]
bpm_x = output_val.keys()[0:len(ch_x)]
bpm_y = output_val.keys()[len(ch_x):len(output_val.keys())]

if make_plot:
    plt.figure()
    plt.subplots_adjust(wspace=0.4, hspace=0.4)
    plt.subplot(2, 2, 1)
    plt.plot(res[bpm_x] * 1e3, alpha=0.5)
    plt.xlabel('N. shot')
    plt.ylabel('Corr discrepancy x (mA)')
    plt.subplot(2, 2, 2)
    plt.plot(res[bpm_y] * 1e3, alpha=0.5)
    plt.ticklabel_format(style='scientific')
    plt.xlabel('N. shot')
    plt.ylabel('Corr discrepancy y (mA)')
    plt.subplot(2, 2, 3)

    std_x = []
    for i in np.arange(0, len(bpm_x)):
        plt.hist(res[bpm_x[i]] * 1e3, bins=20, alpha=0.5)
        std_x.append(np.std(res[bpm_x[i]] * 1e3))
    plt.ticklabel_format(style='scientific')
    plt.ylabel('Counts')
    plt.xlabel('Corr discrepancy x (mA)')
    plt.subplot(2, 2, 4)
    std_y = []
    for i in np.arange(0, len(bpm_y)):
        plt.hist(res[bpm_y[i]] * 1e3, bins=20, alpha=0.5)
        std_y.append(np.std(res[bpm_y[i]] * 1e3))
    plt.ylabel('Counts')
    plt.xlabel('Corr discrepancy y (mA)')
    plt.ticklabel_format(style='scientific')
    # plt.show()
    plt.savefig(output_path + 'error.png')

    plt.figure()
    plt.plot(std_x, 'o-', label='x')
    plt.plot(std_y, 'o-', label='y')
    plt.ylabel('$\Delta$ I (mA)')
    plt.xlabel('Correctors')
    plt.legend()
    # plt.show()
    plt.savefig(output_path + 'error_2.png')

if make_plot:
    plt.figure()
    plt.tight_layout()
    hspace = .4
    plt.subplots_adjust(hspace=hspace)
    plt.subplot(2, 1, 1)
    for i in np.arange(0, len(bpm_x)):
        yf_x = fft(np.array(res[bpm_x[i]]), 2 ** 14)
    plt.plot(np.abs(yf_x))
    plt.ticklabel_format(style='scientific')
    plt.ylabel('fft corr x')
    plt.xlabel('f (arb. units)')
    plt.subplot(2, 1, 2)
    for i in np.arange(0, len(bpm_y)):
        yf_y = fft(np.array(res[bpm_y[i]]), 2 ** 14)
        plt.plot(np.abs(yf_y))
    plt.ylabel('fft corr y')
    plt.xlabel('f (arb. units)')
    plt.ticklabel_format(style='scientific')
    # plt.show()
    plt.savefig(output_path + 'no_save.png')

# I changed to the absolute error (Simona)

# rel_error_table = np.round(rel_error.quantile([0.5, 0.75, 0.9, 0.95, 0.99]).rename(index={
rel_error_table = np.round(res.quantile([0.5, 0.75, 0.9, 0.95, 0.99]).rename(index={
    0.5: '50%',
    0.7: '70%',
    0.75: '75%',
    0.9: '90%',
    0.95: '95%',
    0.99: '99%',
}), decimals=5)

rel_error_table.describe()

if make_plot:
    plt.figure()
    plt.plot(1e3 * res.describe(include='all').loc['mean'].values, label='Mean')
    plt.plot(1e3 * res.describe(include='all').loc['std'].values, label='Std')
    plt.ylabel('$\Delta$ I (mA)')
    plt.xlabel('Correctors')
    plt.legend()
    # plt.show()
    plt.savefig(output_path + 'describe.png')

    plt.figure(figsize=(8, 10))
    plt.subplots_adjust(wspace=0.5, hspace=0.4)
    plt.subplot(2, 2, 1)
    plt.hist(np.abs(res).describe(include='all').loc['min'].values * 1e3, bins=50, label='Min', alpha=0.5)
    plt.ylabel('Counts')
    plt.xlabel('$\Delta$ I (mA)')
    plt.legend()
    plt.subplot(2, 2, 2)
    plt.hist(np.abs(res).describe(include='all').loc['max'].values * 1e3, bins=50, label='Max', alpha=0.5)
    plt.xlabel('$\Delta$ I (mA)')
    plt.ylabel('Counts')
    plt.legend()

    plt.subplot(2, 2, 3)
    plt.hist(res.describe(include='all').loc['mean'].values * 1e3, bins=50, label='mean', alpha=0.5)
    plt.legend()
    plt.ylabel('Counts')
    plt.subplot(2, 2, 4)
    plt.hist(res.describe(include='all').loc['std'].values * 1e3, bins=50, label='std', alpha=0.5)
    plt.xlabel('$\Delta$ I (mA)')
    plt.ylabel('Counts')
    plt.legend()

    plt.xlabel('$\Delta$ I (mA)')
    plt.ylabel('Counts')
    # plt.show()
    plt.savefig(output_path + 'describe_2.png')

# rel_error_table.describe(include='all').lookup
# display(Latex('Total error over the correctors and validation shots: ' + str
#    (np.round(np.mean(res.describe(include='all').loc['mean'].values *1e3 *100) ) /100) + '$\pm$' + str
#    (np.round(np.mean(res.describe(include='all').loc['std'].values *1e3 *100) ) /100) + ' mA'))


np.sum(np.histogram(res.describe(include='all').loc['min'].values * 1e3, bins=34)[0])
np.sum(np.histogram(res.describe(include='all').loc['max'].values * 1e3, bins=34)[0])

# RELATIVE VARIATION

if make_plot:
    plt.figure()
    plt.subplot(2, 1, 1)
    plt.hist(rel_error_table.describe(include='all').loc['min'].values * 1e3, bins=50, label='Min', alpha=0.5)
    plt.hist(rel_error_table.describe(include='all').loc['max'].values * 1e3, bins=50, label='Max', alpha=0.5)
    plt.ylabel('Counts')
    plt.legend()

    plt.subplot(2, 1, 2)
    plt.hist(rel_error_table.describe(include='all').loc['mean'].values * 1e3, bins=50, label='mean', alpha=0.5)
    plt.hist(rel_error_table.describe(include='all').loc['std'].values * 1e3, bins=50, label='std', alpha=0.5)
    plt.ylabel('Counts')
    plt.legend()

    plt.xlabel('Delta I (mA)')
    plt.ylabel('Counts')
    # plt.show()
    plt.savefig(output_path + 'hist.png')

# rel_error_table.describe(include='all').lookup
# print('Std mean error: ' + str(np.std(rel_error_table.describe(include='all').loc['mean'].values*1e3)))
# display(Latex('Total error over the correctors and validation shots: ' + str
#    (np.round(np.mean(rel_error_table.describe(include='all').loc['mean'].values *1e3 *100) ) /100) + '$\pm$' + str
#    (np.round(np.mean(rel_error_table.describe(include='all').loc['std'].values *1e3 *100) ) /100) + ' mA'))


# Save the model

# import tensorflow dependencies
# from tensorflow.keras.models import Sequential, model_from_json
# from tensorflow.keras.layers import Dense

'''diction = {}
if os.path.isfile('Results_loss.h5'):

	Loss_file = pd.read_hdf('Results_loss.h5','results') 

	loss_final_file = Loss_file['Loss'].iloc[-1]
	val_loss_final_file = Loss_file['Val_loss'].iloc[-1]

	os.system('rm Results_loss.h5')

	print('Loss: ' + str(loss_final_file))
	print('Val loss: ' + str(val_loss_final_file))
	print('Results file read')

	if history.history['val_loss'][-1]<1.1*history.history['loss'][-1] and history.history['loss'][-1]<loss_final_file:

		#I save the model, and the Results file
		surr.save('my_model_try.keras')
		print('Model saved (my_model_try.keras)')

		diction['Loss'] = history.history['loss']
		diction['Val_loss'] = history.history['val_loss']

	#else:
	#	diction['Loss'] = [1e5, 1e5]
	#	diction['Val_loss'] = [1e5, 1e5]
	#	print('No better result')

	time.sleep(3)
else:
	diction['Loss'] = history.history['loss']
	diction['Val_loss'] = history.history['val_loss']

df = pd.DataFrame(data=diction)
df.to_hdf('Results_loss.h5', key='results', mode='w')
print('Results_loss.h5 modified')
print('------------------------')

'''



# Based on https://www.tensorflow.org/tutorials/keras/save_and_load


hf = h5py.File(output_path + 'Data_simulation.h5', 'w')
hf.create_dataset('loss', data=history['loss'])
hf.create_dataset('val_loss', data=history['val_loss'])
hf.create_dataset('res', data=res)
hf.close()
print('Simulation data file saved (Data_simulation.h5)')
