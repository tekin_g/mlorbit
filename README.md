# Code Written by Güney during Internship

Most of the codebase is based on ML_train.py by Simona Bettoni with minimal changes to inputs and outputs.

## Datset Generation

 Relevant File is [Create_test_Dataset.py](Create_test_Dataset.py). Use as follows:
1. Generate the necessary data as multiple .h5 in a folder.
2. Set the configuration variables:


```python
model_pyAT = True  # A flag to indicate whether to use the pyAT model
corr_version = True  # A flag to indicate if correctors are available
datset_size = 200000  # Size of the dataset to collect from files
invert = False  # A flag to indicate whether to invert the order of the files 

# Path to the directory containing the data files
my_path = 'data/Network_Corr/Run_3'
# Output filename based on dataset size
outname = "run_3_casecorr_{}.h5".format(datset_size)
```

3. run the script
Notes on the flags:

|     Flag     | Note                                                                                                                                                                                                                                                                                                                                                 |
|:------------:|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| corr_version | This has to be true if correctors should be available in the final dataset. If no correctors are found in the source files, a combined dataset should still be generated. This combined datset would then only have the BPM values. This usecase is untested but should work in theory.                                                              |
|    invert    | invert is used to invert the order of the files in the path. This allows us to take datapoints that were not in the training dataset. This only works if the sizes of desired training and test datasets combined are less than the amount of data available. Else there will be an overlap. The flag also adds a "test_" prefix to the output file. |
|   outname    | {} is replaced by dataset_size.                                                                                                                                                                                                                                                                                                                      |

## Network 1 & 2
[tune_normal_network.py](tune_normal_network.py) is used to train / tune the network 1.

[tune_corr_network.py](tune_corr_network.py) is used for network 2.

Usage:
1. Use the previous chapter to generate a dataset. 
2. Set the desired flags in the script
```python
make_plot = True # use the same flag. In case of a cluster run, savefig

dataset = "run3_dataset_400000.h5"
```

```python
n_epochs = 500

n_depths = [2]# random.randint(np.min(n_depth_loop),np.max(n_depth_loop))
n_widths = [230]
n_batch_size = 512 
l_rates = [5e-5] 
l2_reg_pen_loops = [1e-8] 
internal_dropout_rates = [0.1]
dataset_sizes = [-1]
normalization = False

```
3. Run the script
4. Output will be a timestamped directory (such as "2024-07-22-163506") that has the following structure:
```
2024-07-22-163506
├── 2_230_0.009_0.001_0.2_-1
├── 2_230_0.009_0.001_0.4_-1
├── 2_230_0.009_1e-05_0.2_-1
├── 2_230_0.009_1e-05_0.4_-1
├── 2_230_0.01_0.001_0.2_-1
├── 2_230_0.01_0.001_0.4_-1
├── 2_230_0.01_1e-05_0.2_-1
├── 2_230_0.01_1e-05_0.4_-1
└── Limits_scale.h5
```
The limits_scale.h5 contains the scaling values for input and output. Other directory contain the specific grid search result. The names are generated with the following pattern:

_depth_ _ _width_ _ _learning rate_ _ _l2 reg. rate_ _ _dropout rate_ _ _dataset size_

5. To analyse the output you can use [interpret_grid_search.ipynb](tune_tests/interpret_grid_search.ipynb)

Notes:

|           Flag            | Note                                                                                                                                                                                                                                                                  |
|:-------------------------:|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|       dataset_size        | This flag can be used to limit the dataset used during training independent from the actual dataset size in the .h5 file. -1 uses the whole dataset.                                                                                                                  |
|       normalization       | This flags adds a _normalization_ layer at the beginning of the network. Unlike batch normalization, this layer does constant normalization. Might be useful in some cases.                                                                                           |
| all flags that are arrays | These flags are used to generate the grid search. All possible combinations are tried. Make sure that the runtime of the cluster job is high enough. Results saved for each node in the grid independently, so not all progress will be lost in case of interruption. |

## Autoencoder Network
Scripts are: 
- [crazy_model.py](crazy_model.py) defines the network.
- [train_crazy_model.py](train_crazy_model.py) training without correctors.
- [train_crazy_model_corr.py](train_crazy_model_corr.py) training with correctors. 
- [testing.ipynb](testing.ipynb) testing for networks without correctors.
- ~~[tune_crazy_model.py](tune_crazy_model.py)~~ broken

Similar process as Network 1 & 2:
- Each run is a single training so results are directly in the base directory (i.e. timestamp).
- Everything is as similar as possible

Flags:
input & output
```python
make_plot = True  # use the same flag. In case of a cluster run, savefig
dataset = "run_5_case3b_40000.h5"
```
general settings
```python
n_epochs = 100
n_batch_size = 128
l_rate = 5e-4
l2_reg_pen_loop = 1e-7
internal_dropout_rate = 0.3
alpha = 2.0  # ratio of reconstruction loss to corrector loss
```

faulty BPM specific settings
```python
no_data_token = -5  #Token that represnts the broken BPM
ratio_input_dropout = 0  # [0,1] ratio of the datapoints that contain broken BPMs
dropout_column_no = 230  # how many BPMs can actually be broken ( in each datapoint only a single one is actually
# broken, this just determines how many BPMs to select from when marking as broken)
```

architecture shape settings
```python
##### Settings for the corrector calculating part of the network
n_depth = 2
n_width = 230
#### Settings for the AutoEncoder
latent_dim = 30
encoder_depth = 2
```
Alpha sets the ratio of BPM reconstruction MSE and corrector MSE:

$$
Loss = MSE(BPM_{rec} - BPM_{true}) + \alpha * MSE(corr_{inf} - corr_{true})
$$

## Run on GPUs
Easiest way is to use a container! TF kind of sucks without containers. Merlin supports [Apptainer](https://apptainer.org/docs/user/main/) containers.
Steps:
1. You need a .def file. An example is in [tf2_image_gpu.def](tf2_image_gpu.def).
```shell
# These two lines tell it use the ready made tensorflow 2.16.1 container with GPU support. 
Bootstrap: docker
From: tensorflow/tensorflow:2.16.1-gpu

%post
# The code here is run in the container before it is finalized.
# Here you can install new libraries. 
# Afterwards you can not change the container.
pip install matplotlib
pip install pandas
pip install scipy
pip install scikit-learn
pip install tables

%environment
# set flags in the container if necessary
%files
# copy files into the container if necessary
# you do not need to copy your code. 
%runscript
    echo "Container is designed to be used using apptainer shell!"
```
2. Build the container. This generates a .sif file that can be copied to the cluster. Once built, the .sif file can not be changed easily.
```shell
apptainer build container.sif tf2_image_gpu.def
```
3. Now you are free to use the container:
   - Enter a shell in the container:
    ```shell
    apptainer shell --nv container.sif
    ```
   - Run a command in the container
    ```shell
    apptainer exec --nv container.sif
    ```
   During these commands the files at the current location are still available in the container. Any changes made to these will be permanent. Any changes made to the container are reset afterward.
4. In Merlin use:

```shell
#SBATCH --partition=gwendolen         # Or 'gmerlin' for gmerlin gpu partition
#SBATCH --account=gwendolen       # Or 'gwendolen-long' for 8 hour time limit. Or 'gpu-short' for gmerlin
#SBATCH --cluster=gmerlin6      # Required for GPU
#SBATCH --gpus=1                # Multi GPU not supported
#SBATCH --cpus-per-gpu=1        # Request CPU resources
#SBATCH --mem-per-cpu=20G
#SBATCH --time=2:00:00       # Define max time job will run

#cd /data/project/general/ML/guney depends on where you are

srun apptainer exec --nv container.sif python MLOrbit/tune_normal_network.py
```
## Available models
Models are available in the cluster under 
/data/projects/general/ML/out/ _time stamp_

|               Time Stamp               | Explanation                                                                                                                                                     |
|:--------------------------------------:|-----------------------------------------------------------------------------------------------------------------------------------------------------------------|
|           2024-06-17-213649            | Autoencoder                                                                                                                                                     |
|           2024-06-18-160112            | Autoencoder **best so far**                                                                                                                                     |
|           2024-06-24-135051            | Autoencoder                                                                                                                                                     |
|           2024-07-03-193857            | Autoencoder with different misalignment in dataset (very bad)                                                                                                   |
|           2024-07-10-135245            | Network 1 grid search for minimum datapoints required for training. Results in [interpret_grid_search.ipynb](tune_tests%2Finterpret_grid_search.ipynb).         |
|           2024-07-17-142337            | Grid Search for best Network 2. Focused on network shape                                                                                                        |
|           2024-07-17-152207            | Grid Search for best Network 2. . **Best in 2_460_0.0005_1e-08_0.2_-1**.                                                                                        |
|           2024-07-22-163506            | Transfer learning trials. No Good results min mean delta I 2mA. Results in [interpret_grid_search_trans.ipynb](tune_tests%2Finterpret_grid_search_trans.ipynb). |
|           2024-07-25-130241            | Best for Network 1 with many epochs trained.                                                                                                                    |
|           2024-07-29-174112            | Best Network 2 Result in [interpret_grid_search_corr.ipynb](tune_tests%2Finterpret_grid_search_corr.ipynb)                                                      |                                                                                                                                                     
| 2024-07-29-192118 to 2024-07-29-202615 | Grid Searches for Network 2 with focus on only 20000 datapoints being available. **No good results.**                                                           |

## Thanks
Thanks to Simona for the codebase and to the merlin/gwendolen team for making such resources available. Without gwendolen this project would have cost at least an extra 350$ for the compute time alone.


